public class Animal {

        /*
        Activity 2:

        Create a class called Animal with the following attributes:
        name - string
        color - string

        Add a default and parameterized constructor for the class.
        Add getters and setters for the name and color attributes.
        Add public method called call() which prints a message and the value of the name attribute.
            This should not return anything.
            Message: "Hi! My name is <valueOfName>"
        In the main method of the Main class, create a new instance of the Animal class and store it in a variable called animal1.
            - initialize values for the animal instance.
            - use the call() method of the animal
        -Add your update in your local git repository in your s4 folder
        -Push to git with the commit message of Add activity 2 code.

    */

    private String name;
    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Animal(){}

    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }

    //method
    public void call(){
        System.out.println("Hi my name is " + this.name);
    }
}
