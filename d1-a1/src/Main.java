public class Main {
    //public String sample = "Sample";

    public static void main(String[] args) {
//        Main newSample = new Main();
//        System.out.println(newSample.sample);
        Car car1 = new Car();
//      System.out.println(car1.brand);
//      System.out.println(car1.make);
//      System.out.println(car1.price);
//      car1.make = "Veyron";
//      car1.brand = "Bugatti";
//      car1.price = 2000000;
//      System.out.println(car1.brand);
//      System.out.println(car1.make);
//      System.out.println(car1.price);
        //Each instance of a class should be independent from one another especially their properties. However, since coming from the same class, they may have the same methods.
        Car car2 = new Car();
//       car2.make = "Tamaraw FX";
//       car2.brand = "Toyota";
//       car2.price = 450000;
//       System.out.println(car2.make);
//       System.out.println(car2.brand);
//       System.out.println(car2.price);
        //car2.owner = "Mang Jose";//We cannot add a new property not described in the class.
        /*
            Mini-Activity:

            Create a new instance of the Car class and save it in a variable called car3.
            Access the properties of the instance and update its values.
                make = String
                brand = String
                price = int
             You can come up with your own values.
             Print the values of each property of the instance.
        */
        Car car3 = new Car();
//        car3.make = "Civic";
//        car3.brand = "Honda";
//        car3.price = 1000000;
//        System.out.println(car3.make);
//        System.out.println(car3.brand);
//        System.out.println(car3.price);
        car3.start();
        car1.start();
        car2.start();
        car1.setMake("Veyron");
        System.out.println(car1.getMake());
        //car1.setBrand("Bugatti");
        System.out.println(car1.getBrand());
        car1.setPrice(2000000);
        System.out.println(car1.getPrice());
        //When creating a new instance of a class:
        //new keyword allows us to create a new instance
        //Car() - constructor of our class - without arguments - default constructor - which Java can define for us.
        Car car4 = new Car();
        System.out.println(car4.getMake());
        System.out.println(car4.getBrand());
        System.out.println(car4.getPrice());

        Driver driver1 = new Driver("John",34,"Quezon City");
        System.out.println("Details of driver1:");
        System.out.println(driver1.getName());
        System.out.println(driver1.getAge());
        System.out.println(driver1.getAddress());

        Car car5 = new Car("Corolla","Toyota", 500000,driver1);
        System.out.println(car5.getMake());
        System.out.println(car5.getBrand());
        System.out.println(car5.getPrice());
        System.out.println(car5.getCarDriverName());

        Animal animal1 = new Animal("Bantay","Red");
        animal1.call();
        Dog newDog = new Dog();
        System.out.println(newDog.getName());
        newDog.setName("Balto");
        System.out.println(newDog.getName());
        Dog newDog2 = new Dog("Whitey","White","Husky");
        System.out.println(newDog2.getName());
        newDog2.call();
        System.out.println(newDog2.getDogBreed());
        newDog2.greet();
        Person newPerson = new Person();
        newPerson.run();
        newPerson.sleep();
        newPerson.morningGreet();
        newPerson.holidayGreet();
        StaticPoly newStaticPoly = new StaticPoly();
        System.out.println(newStaticPoly.addition(5,5));
        System.out.println(newStaticPoly.addition(15,5,3));
        System.out.println(newStaticPoly.addition(15.00,25.56));
        Child newChild = new Child();
        newChild.speak();
    }

}
